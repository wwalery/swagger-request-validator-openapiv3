
validation.request.path.missing=No API path found that matches request '%s'.
validation.request.operation.notAllowed=%s operation not allowed on path '%s'.
validation.request.body.unexpected=No request body is expected for %s on path '%s'.
validation.request.body.missing=%s on path '%s' requires a request body. None found.
validation.request.security.missing=%s on path '%s' requires security parameters. None found.

validation.request.parameter.header.missing=Header parameter '%s' is required on path '%s' but not found in request.
validation.request.parameter.query.missing=Query parameter '%s' is required on path '%s' but not found in request.
validation.request.parameter.missing=Parameter '%s' is required but is missing.
validation.request.parameter.enum.invalid=Value '%s' for parameter '%s' is not allowed. Allowed values are <%s>.
validation.request.parameter.invalidFormat=Value '%s' for parameter '%s' does not match type '%s' with format '%s'.

validation.request.parameter.number.belowMin=Value '%s' for parameter '%s' less than allowed min value %f.
validation.request.parameter.number.belowExclusiveMin=Value '%s' for parameter '%s' less than or equal to allowed exclusive min value %f.
validation.request.parameter.number.aboveMax=Value '%s' for parameter '%s' greater than allowed max value %f.
validation.request.parameter.number.aboveExclusiveMax=Value '%s' for parameter '%s' greater or equal to allowed exclusive max value %f.
validation.request.parameter.number.multipleOf=Value %s for parameter '%s' not a multiple of %s.

validation.request.parameter.collection.invalidFormat=Parameter '%s' expected collection format of '%s' but '%s' was used instead.
validation.request.parameter.collection.tooManyItems=Parameter '%s' accepts a maximum of %d items. Found %d.
validation.request.parameter.collection.tooFewItems=Parameter '%s' accepts a minimum of %d items. Found %d.
validation.request.parameter.collection.duplicateItems=Parameter '%s' does not allow duplicate values.
validation.request.parameter.string.patternMismatch=Parameter '%s' does not match the required pattern '%s'.
validation.request.parameter.string.tooShort=Parameter '%s' is too short. Minimum length: %d.
validation.request.parameter.string.tooLong=Parameter '%s' is too long. Maximum length: %d.
validation.request.parameter.string.date.invalid=Parameter '%s' is not a valid ISO8601 date.
validation.request.parameter.string.dateTime.invalid=Parameter '%s' is not a valid ISO8601 date time.
validation.request.parameter.string.uuid.invalid=Parameter '%s' is not a valid UUID.
validation.request.parameter.string.email.invalid=Parameter '%s' is not a valid email.
validation.request.parameter.string.ipv4.invalid=Parameter '%s' is not a valid IPv4 address.
validation.request.parameter.string.ipv6.invalid=Parameter '%s' is not a valid IPv6 address.
validation.request.parameter.string.uri.invalid=Parameter '%s' is not a valid URI.

validation.request.contentType.invalid=Request Content-Type header '%s' is not a valid media type
validation.request.contentType.notAllowed=Request Content-Type header '%s' does not match the 'consumes' types. Must be one of: '%s'.
validation.request.accept.invalid=Request Accept header '%s' is not a valid media type
validation.request.accept.notAllowed=Request Accept header '%s' does not match the 'produces' types. Must be one of: '%s'.

validation.response.status.unknown=Response status %d not defined for path '%s'.
validation.response.body.missing=%s on path '%s' defines a response schema but no response body found.
validation.response.contentType.invalid=Response Content-Type header '%s' is not a valid media type
validation.response.contentType.notAllowed=Response Content-Type header '%s' does not match the 'produces' types. Must be one of: '%s'.
validation.response.header.missing=Header '%s' is expected on path '%s' but not found in response.

validation.schema.invalidJson=Unable to parse JSON - %s.
validation.schema.unknownError=An error occurred during schema validation - %s.

# Other schema validation messages come from the json-schema-validation library
# https://github.com/daveclayton/json-schema-validator
# They are emitted with keys of the form 'validation.schema.<keyword>'
# where <keyword> corresponds to a JSON schema validation keyword.
# See http://json-schema.org/latest/json-schema-validation.html for keywords that may occur.
#
# For example
#  - validation.schema.required: A required field is missing
#  - validation.schema.type: A field has the incorrect type